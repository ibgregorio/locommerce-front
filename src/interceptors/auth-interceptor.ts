import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { WebstorageService } from "app/service/webstorage.service";
import { environment } from "environments/environment";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(public storage: WebstorageService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {

        let localUser = this.storage.getLocalUser();

        let baseUrlLength = environment.baseUrl.length;
        let requestToApi = req.url.substring(0, baseUrlLength) == environment.baseUrl;
        
        if (localUser && requestToApi) {
            const authReq = req.clone({headers: req.headers.set('Authorization', 'Bearer ' + localUser.token)});
            return next.handle(authReq);
        } else {
            return next.handle(req);
        }
    }
}

export const AuthInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
};