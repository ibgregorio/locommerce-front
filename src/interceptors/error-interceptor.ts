import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Observable } from "rxjs/Rx";
import { WebstorageService } from "../app/service/webstorage.service";
import { FieldMessage } from "../models/fieldmessage";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(public storage: WebstorageService, public toaster: ToastrService){
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {   
        return next.handle(req)
        .catch((error, caught) => {
            let errorObj = error;
            if (errorObj.error) {
                errorObj = errorObj.error;
            }

            if (!errorObj.status) {
                errorObj = JSON.parse(errorObj);
            }

            switch (errorObj.status) {
                case 401:
                    this.handleUnauthorized();
                    break;
                case 403:
                    this.handleForbidden();
                    break;
                case 422:
                    this.handleUnprocessableEntity(errorObj);
                    break;
                default:
                    this.handleDefaultError(errorObj);
            }
            
            return Observable.throw(errorObj);
        }) as any;
    }

    handleForbidden() {
        this.storage.setLocalUser(null);
    }

    handleUnauthorized() {
        this.toaster.error(null, "E-mail ou senha incorretos!")
    }

    handleUnprocessableEntity(errorObj) {
        this.listErrors(errorObj.errors);
    }

    handleDefaultError(errorObj) {
        this.toaster.error(null, errorObj.message);
    }

    private listErrors(messages : FieldMessage[]) {

        for (let i = 0; i < messages.length; i++) {
            this.toaster.warning(`${messages[i].field}: ${messages[i].msg}`, 'Erro de validação');
        }
    }
}

export const ErrorInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
};