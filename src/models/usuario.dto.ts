import { EnderecoDTO } from "./endereco.dto";
import { LojaDTO } from "./loja.dto";

export interface UsuarioDTO {
    id : number;
    nome : string;
    email : string;
    cpf: string;
    numeTel: string;
    numeCelular: string;
    enderecos?: EnderecoDTO[];
    loja?: LojaDTO;
}