import { CategoriaDTO } from "./categoria.dto";
import { EnderecoDTO } from "./endereco.dto";

export interface LojaDTO {
    id: number;
    nomeFantasia: string;
    cnpj: string;
    descricao: string;
    justificativaReprovacao: string;
    endereco: EnderecoDTO;
    categoria: CategoriaDTO;
    idCategoria: string;
    situacao: string;
}