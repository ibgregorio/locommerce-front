import { CategoriaDTO } from "./categoria.dto";

export interface ProdutoDTO {
    id: number;
    nome: string;
    detalhes: string;
    qtdEstoque: number;
    preco: number;
    categoria: CategoriaDTO;
}