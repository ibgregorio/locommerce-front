import { EnderecoDTO } from "./endereco.dto";

export interface LojaNovaDTO {
    id : number;
    nomeFantasia : string;
    cnpj : string;
    idCategoria: string;
    endereco: EnderecoDTO;
    idProprietario: number
}