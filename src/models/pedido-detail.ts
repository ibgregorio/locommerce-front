import { EnderecoDTO } from "./endereco.dto";
import { PedidoItemDetail } from "./pedido-item-detail";
import { UsuarioDTO } from "./usuario.dto";

export interface PedidoDetail {
    id: number;
    status: string;
    dataPedido: string;
    comprador: UsuarioDTO;
    enderecoEntrega: EnderecoDTO;
    formaPgto: string;
    itensPedido: PedidoItemDetail[];
}