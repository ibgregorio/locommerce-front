export interface StoredUser {
    token: string;
    email: string;
    perfil: string;
}