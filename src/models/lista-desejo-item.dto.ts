import { ProdutoDTO } from "./produto.dto";

export interface ListaDesejoItemDTO {
    produto: ProdutoDTO;
}