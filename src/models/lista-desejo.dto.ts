import { ListaDesejoItemDTO } from "./lista-desejo-item.dto";

export interface ListaDesejoDTO {
    id: number;
    nomeLista: string;
    itensDesejo: ListaDesejoItemDTO[];
}