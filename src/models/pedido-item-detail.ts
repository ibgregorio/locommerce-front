import { ProdutoDTO } from "./produto.dto";

export interface PedidoItemDetail {
    quantidade : number;
    preco: number;
    produto : ProdutoDTO;
}