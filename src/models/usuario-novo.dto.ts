import { EnderecoDTO } from "./endereco.dto";
import { LojaNovaDTO } from "./loja-nova.dto";

export interface UsuarioNovoDTO {
    id : number;
    nome : string;
    email : string;
    senha: string;
    cpf: string;
    numeTel: string;
    numeCelular: string;
    endereco?: EnderecoDTO;
    loja?: LojaNovaDTO;
    tipoUsuario: number;
}