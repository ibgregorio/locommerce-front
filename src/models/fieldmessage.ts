export interface FieldMessage {
    field: string;
    msg: string;
}