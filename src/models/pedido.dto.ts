import { PkRefDTO } from "./pkref.dto";
import { PedidoItemDTO } from "./pedido-item.dto";

export interface PedidoDTO {
    comprador: PkRefDTO;
    enderecoEntrega: PkRefDTO;
    formaPgto: string;
    itensPedido: PedidoItemDTO[];
}