export interface AvaliacaoDTO {
    nota: number;
    idLojaAvaliada: number;
    idUsuarioAvaliacao: number;
}