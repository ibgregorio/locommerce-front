import { PkRefDTO } from "./pkref.dto";

export interface PedidoItemDTO {
    quantidade : number;
    produto : PkRefDTO;
}