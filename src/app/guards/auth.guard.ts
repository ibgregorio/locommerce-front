import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from 'app/service/auth.service';
import { WebstorageService } from 'app/service/webstorage.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService,
    private storage: WebstorageService,
    private toaster: ToastrService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.storage.getLocalUser();
    if (currentUser) {

        if (!this.authService.isAdmin() && route.url[0].path === 'admin') {
          this.toaster.error(null, 'Acesso não autorizado');
          this.router.navigate(['/']);
          return false;
        }
        
        return true;
    }
    
    this.toaster.warning(null, 'Realize o login para acessar esta funcionalidade');
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
  
}
