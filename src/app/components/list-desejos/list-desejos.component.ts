import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ItemDesejoService } from 'app/service/item-desejo.service';
import { ListDesejosService } from 'app/service/list-desejos.service';
import { ListaDesejoItemDTO } from 'models/lista-desejo-item.dto';
import { ListaDesejoDTO } from 'models/lista-desejo.dto';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-desejos',
  templateUrl: './list-desejos.component.html',
  styleUrls: ['./list-desejos.component.scss']
})
export class ListDesejosComponent implements OnInit {

  listasDesejos: ListaDesejoDTO[];
  formListaDesejo: FormGroup;

  modoCrud: string;
  selectedLista: ListaDesejoDTO;

  constructor(private modalService: NgbModal,
              private listDesejosService: ListDesejosService,
              private itemDesejoService: ItemDesejoService,
              private formBuilder: FormBuilder,
              private toaster: ToastrService) { 
    this.formListaDesejo = this.formBuilder.group({
      nomeLista: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]]
    });
  }

  ngOnInit(): void {
    this.loadListas();
  }
  
  loadListas() {
    this.listDesejosService.findByUsuarioLogado()
        .subscribe(response => {
          this.listasDesejos = response;      
        },
        error => {});
  }

  verProdutosLista(modalId, lista: ListaDesejoDTO) {   
    this.selectedLista = lista;

    this.modalService.open(modalId, { size: 'lg' });
  }

  openModal(modalId, modo, lista) {   
    this.modoCrud = modo;

    if (lista) {
      this.selectedLista = lista;

      if (modo === 'E') {
        this.formListaDesejo.patchValue(lista);
      }
    }
    
    this.modalService.open(modalId, { size: 'lg' });
  }

  closeModal() {
    this.modoCrud = null;
    this.selectedLista = null;
    this.formListaDesejo.reset();
    this.modalService.dismissAll();
  }

  salvarLista() {
    if (this.formListaDesejo.invalid) {
      return;
    }

    if (this.modoCrud === 'N') {

      this.listDesejosService.insert(this.formListaDesejo.value)
        .subscribe(response => {
          this.toaster.success(null, 'Nova lista de desejos criada!');
          this.loadListas();
          this.closeModal();
        },
        error => {});
    } else {
      this.listDesejosService.update(this.formListaDesejo.value, this.selectedLista)
        .subscribe(response => {
          this.toaster.success(null, 'Lista de Desejos atualizada com sucesso!');
          this.loadListas();
          this.closeModal();
        },
        error => {});
    }
  }

  excluirLista() {
    this.listDesejosService.delete(this.selectedLista.id.toString())
        .subscribe(response => {
          this.toaster.success(null, 'A lista selecionada foi removida');
          this.loadListas();
          this.closeModal();
        },
        error => {
          this.closeModal();
        });
  }

  excluirItem(item: ListaDesejoItemDTO) {
    this.itemDesejoService.delete(this.selectedLista.id.toString(), item.produto.id.toString())
        .subscribe(response => {
          this.toaster.success(null, 'O produto foi removido da lista de desejos');
          this.loadListas();
          this.closeModal();
        },
        error => {
          this.closeModal();
        });
  }

}
