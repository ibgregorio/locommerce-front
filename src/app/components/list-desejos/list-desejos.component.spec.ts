import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDesejosComponent } from './list-desejos.component';

describe('ListDesejosComponent', () => {
  let component: ListDesejosComponent;
  let fixture: ComponentFixture<ListDesejosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDesejosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDesejosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
