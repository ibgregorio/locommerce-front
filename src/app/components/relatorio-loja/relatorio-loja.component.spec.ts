import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatorioLojaComponent } from './relatorio-loja.component';

describe('RelatorioLojaComponent', () => {
  let component: RelatorioLojaComponent;
  let fixture: ComponentFixture<RelatorioLojaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatorioLojaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatorioLojaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
