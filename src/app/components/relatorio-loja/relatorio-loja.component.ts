import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { jsPDF } from 'jspdf';
import * as htmlToImage from 'html-to-image';

@Component({
  selector: 'app-relatorio-loja',
  templateUrl: './relatorio-loja.component.html',
  styleUrls: ['./relatorio-loja.component.scss']
})
export class RelatorioLojaComponent implements OnInit {
  public vendasChartData: ChartDataSets[] = [
    { data: [115, 390, 200, 387, 593, 550, 473, 351, 291, 387, 593, 970, 832], label: 'Vendas Realizadas' }
  ];
  public vendasChartLabels: Label[] = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ];
  public vendasChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }
      ]
    },
    annotation: {},
  };
  public vendasChartColors: Color[] = [
    { // red
      backgroundColor: 'rgba(0,255,0,0.3)',
      borderColor: 'green',
      pointBackgroundColor: '#000',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public vendasChartLegend = false;
  public vendasChartType: ChartType = 'line';

  public avaliacoesChartLabels:string[] = ["1 estrela", "2 estrelas", "3 estrelas", "4 estrelas", "5 estrelas"];
  public avaliacoesChartData:number[] = [5, 5, 15, 45, 30];
  public avaliacoesChartType:string = 'pie';
  public avaliacoesChartOptions:any = {'backgroundColor': [
               "#FF6384",
            "#E7E9ED",
            "#FFCE56",
            "#4BC0C0",
            "#36A2EB"
            ]}

  public categoriaChartLabels:string[] = ["Artesanato", "Calçados", "Para Casa", "Vestuário"];
  public categoriaChartData:number[] = [25, 25, 15, 35];
  public categoriaChartType:string = 'doughnut';
  public categoriaChartOptions:any = {'backgroundColor': [
               "#FF6384",
            "#E7E9ED",
            "#FFCE56",
            "#36A2EB"
            ]}

  public pedidosChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public pedidosChartLabels: Label[] = ['Pendente', 'Aprovado', 'Em Trânsito', 'Cancelado', 'Entregue'];
  public pedidosChartType: ChartType = 'bar';
  public pedidosChartLegend = false;

  public pedidosChartData: ChartDataSets[] = [
    { data: [25, 52, 40, 15, 67]}
  ];

  constructor() { }

  ngOnInit(): void {
  }

gerarPDF() {
  const filename = 'relatório-loja.pdf';
  const pagePrintElement = document.getElementById('printDiv');
  htmlToImage.toPng(pagePrintElement)
  .then( (dataUrl) => {
  const img = new Image();
  img.src = dataUrl;
  const pdf = new jsPDF('p', 'mm', 'a4');
  pdf.setLineWidth(1);
  pdf.addImage(img, 'PNG', 0, 0, 208, img.height);
  pdf.save(filename);
  })
  .catch((error) => {
  console.error('Erro ao gerar o PDF', error);
  });
  }

}

