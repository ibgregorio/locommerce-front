import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CarrinhoService } from 'app/service/carrinho.service';
import { PedidoService } from 'app/service/pedido.service';
import { UsuarioService } from 'app/service/usuario.service';
import { WebstorageService } from 'app/service/webstorage.service';
import { EnderecoDTO } from 'models/endereco.dto';
import { ItemCarrinho } from 'models/item-carrinho';
import { PedidoDTO } from 'models/pedido.dto';
import { UsuarioDTO } from 'models/usuario.dto';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.scss']
})
export class PedidoComponent implements OnInit {

  step: number = 1;
  pedidoId: string;
  
  enderecos: EnderecoDTO[];
  itensCarrinho: ItemCarrinho[];
  pedido: PedidoDTO;
  comprador: UsuarioDTO;
  endereco: EnderecoDTO;

  formPedidoEndereco: FormGroup;
  formTipoPagamento: FormGroup;

  constructor(
    public storage: WebstorageService,
    public usuarioService: UsuarioService,
    public carrinhoService: CarrinhoService,
    public pedidoService: PedidoService,
    public formBuilder: FormBuilder,
    public router: Router) { 

      this.formPedidoEndereco = this.formBuilder.group({
        enderecoEntrega: ['', [Validators.required]]
      });

      this.formTipoPagamento = this.formBuilder.group({
        tipoPagamento: ['', [Validators.required]]
      });
  }

  ngOnInit(): void {
    let localUser = this.storage.getLocalUser();

    if (localUser && localUser.email) {
      this.usuarioService.findByEmail(localUser.email)
        .subscribe(response => {
          this.enderecos = response['enderecos'];

          let cart = this.carrinhoService.getCarrinho();

          this.pedido = {
            comprador: {id: response['id']},
            enderecoEntrega: null,
            formaPgto: null,
            itensPedido: cart.itens.map(x => {return {quantidade: x.quantidade, produto: {id: x.produto.id}}})
          };
        },
        error => {
          if (error.status == 403) {
            this.router.navigate([`/`]);
          }
        });
    } else {
      this.router.navigate([`/`]);
    }
  }

  voltar (endereco: EnderecoDTO) {
    this.step--;
  }

  avancar(endereco: EnderecoDTO) {
    if (this.step === 2) {
      this.setTipoPagamento();
    }

    this.step++;

    if (this.step === 3) {
      this.loadDetailsPedido();
    }
  }

  confirmarPedido() { 
    this.pedidoService.insert(this.pedido)
      .subscribe(response => {
        this.carrinhoService.createOrClearCarrinho();

        this.pedidoId = this.extractPedidoId(response.headers.get('Location'));
      },
      error => {
        if (error.status == 403) {
          this.router.navigate([`/`]);
        }
      });
  }

  setEnderecoEntrega(endereco: EnderecoDTO) {
    this.pedido.enderecoEntrega = {id: endereco.id};
  }

  setTipoPagamento() {
    this.pedido.formaPgto = this.formTipoPagamento.value.tipoPagamento;
  }

  loadDetailsPedido() {
    this.itensCarrinho = this.carrinhoService.getCarrinho().itens;

    this.usuarioService.findById(this.pedido.comprador.id.toString())
      .subscribe(response => {
        this.comprador = response as UsuarioDTO;
        this.endereco = this.findEndereco(this.pedido.enderecoEntrega.id.toString(), response['enderecos'])
      },
      error => {
        this.router.navigate([`/`]);
      });
  }

  total() : number {
    return this.carrinhoService.total();
  }

  private findEndereco(id: string, list: EnderecoDTO[]) : EnderecoDTO {
    let position = list.findIndex(x => x.id.toString() == id);

    return list[position];
  }

  private extractPedidoId(location : string) : string {
    let position = location.lastIndexOf('/');

    return location.substring(position + 1, location.length);
  }  

}
