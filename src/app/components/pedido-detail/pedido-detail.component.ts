import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PedidoService } from 'app/service/pedido.service';
import { PedidoDetail } from 'models/pedido-detail';

@Component({
  selector: 'app-pedido-detail',
  templateUrl: './pedido-detail.component.html',
  styleUrls: ['./pedido-detail.component.scss']
})
export class PedidoDetailComponent implements OnInit {

  pedido: PedidoDetail;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private pedidoService: PedidoService) { }

  ngOnInit(): void {
    let id_selected_pedido = this.route.snapshot.params['id']

    this.pedidoService.findById(id_selected_pedido)
        .subscribe(response => {
          this.pedido = response as PedidoDetail;          
        },
        error => {});
  }

  total() : number {
    let sum = 0;

    for (let i = 0; i < this.pedido?.itensPedido.length; i++) {
      sum += this.pedido.itensPedido[i].produto.preco * this.pedido.itensPedido[i].quantidade;
    }

    return sum;
  }

  voltar() {
    let sum = 0;

    for (let i = 0; i < this.pedido?.itensPedido.length; i++) {
      sum += this.pedido.itensPedido[i].produto.preco * this.pedido.itensPedido[i].quantidade;
    }

    return sum;
  }

}
