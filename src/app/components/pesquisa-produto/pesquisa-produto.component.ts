import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarrinhoService } from 'app/service/carrinho.service';
import { ProdutoService } from 'app/service/produto.service';
import { ProdutoDTO } from 'models/produto.dto';

@Component({
  selector: 'app-pesquisa-produto',
  templateUrl: './pesquisa-produto.component.html',
  styleUrls: ['./pesquisa-produto.component.scss']
})
export class PesquisaProdutoComponent implements OnInit {

  itens: ProdutoDTO[];

  constructor(public produtoService: ProdutoService, public router: Router, public route: ActivatedRoute, public carrinhoService: CarrinhoService) { }

  ngOnInit(): void {
    this.loadResultadosBusca();
  }

  loadResultadosBusca() {
    let categoriaParam = this.route.snapshot.queryParamMap.get('categoria');
    let nomeParam = this.route.snapshot.queryParamMap.get('nome')

    this.produtoService.findProdutos(categoriaParam, nomeParam)
      .subscribe(response => {
        this.itens = response;
      },
      error => {});
  }

  comprarProduto(produto: ProdutoDTO) {
    this.carrinhoService.adicionarProduto(produto, 1);
    this.router.navigate([`/order`]);
  }

  visualizar(produto: ProdutoDTO) {
    this.router.navigate([`/product/${produto.id}`]);
  }

}
