import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LojaService } from 'app/service/loja.service';
import { LojaDTO } from 'models/loja.dto';

@Component({
  selector: 'app-pesquisa-loja',
  templateUrl: './pesquisa-loja.component.html',
  styleUrls: ['./pesquisa-loja.component.scss']
})
export class PesquisaLojaComponent implements OnInit {

  itens: LojaDTO[];

  constructor(public lojaService: LojaService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadResultadosBusca();
  }

  loadResultadosBusca() {
    let categoriaParam = this.route.snapshot.queryParamMap.get('categoria');
    let nomeParam = this.route.snapshot.queryParamMap.get('nome')

    this.lojaService.findLoja(categoriaParam, nomeParam)
      .subscribe(response => {
        this.itens = response;
      },
      error => {});
  }

  visualizar(loja: LojaDTO) {
    this.router.navigate([`/store/${loja.id}`]);
  }

}
