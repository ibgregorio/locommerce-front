import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisaLojaComponent } from './pesquisa-loja.component';

describe('PesquisaLojaComponent', () => {
  let component: PesquisaLojaComponent;
  let fixture: ComponentFixture<PesquisaLojaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisaLojaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisaLojaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
