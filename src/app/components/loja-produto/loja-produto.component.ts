import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'app/service/auth.service';
import { CarrinhoService } from 'app/service/carrinho.service';
import { CategoriaService } from 'app/service/categoria.service';
import { ProdutoService } from 'app/service/produto.service';
import { UsuarioService } from 'app/service/usuario.service';
import { WebstorageService } from 'app/service/webstorage.service';
import { CategoriaDTO } from 'models/categoria.dto';
import { LojaDTO } from 'models/loja.dto';
import { ProdutoDTO } from 'models/produto.dto';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-loja-produto',
  templateUrl: './loja-produto.component.html',
  styleUrls: ['./loja-produto.component.scss']
})
export class LojaProdutoComponent implements OnInit {

  itens: ProdutoDTO[];
  categorias: CategoriaDTO[];

  modoCrud: string;
  selectedProduto: ProdutoDTO;
  formProduto: FormGroup;

  lojaUsuarioLogado: boolean = false;

  constructor(private produtoService: ProdutoService, 
              private router: Router, 
              private route: ActivatedRoute, 
              private storage: WebstorageService,
              public carrinhoService: CarrinhoService,
              public authService: AuthService,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private categoriaService: CategoriaService,
              private usuarioService: UsuarioService,
              private toaster: ToastrService) { 

    this.formProduto = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.maxLength(100)]],
      detalhes: ['', [Validators.required, Validators.maxLength(255)]],
      qtdEstoque: ['', [Validators.required, Validators.min(1)]],
      preco: ['', [Validators.required, Validators.min(1)]],
      idCategoria: ['', [Validators.required]],
      idLoja: [this.route.snapshot.params['id'], []]
    });
  }

  ngOnInit(): void {
    this.loadProdutosLoja();

    if (this.authService.isVendedor()) {
      let id_selected_loja = this.route.snapshot.params['id']
      let lojaVendedor: LojaDTO;

      this.usuarioService.findByEmail(this.storage.getLocalUser().email)
        .subscribe(response => {
          lojaVendedor = response['loja'] as LojaDTO;
          
          if (lojaVendedor.id.toString() === id_selected_loja.toString()) {
            this.lojaUsuarioLogado = true;
          }          
        },
        error => {});
      
        this.categoriaService.findAll()
          .subscribe(response => {
            this.categorias = response;
          },
          error => {});

    }
  }

  loadProdutosLoja() {
    let id_loja = this.route.snapshot.params['id'];

    this.produtoService.loadProdutosFromLoja(id_loja)
      .subscribe(response => {
        this.itens = response;
      },
      error => {});
  }

  comprarProduto(produto: ProdutoDTO) {
    this.carrinhoService.adicionarProduto(produto, 1);
    this.router.navigate([`/order`]);
  }

  visualizar(produto: ProdutoDTO) {
    this.router.navigate([`/product/${produto.id}`]);
  }

  openModal(modalId, modo, produto) {
    this.modoCrud = modo;

    if (modo === 'N') {
      this.formProduto.controls.idLoja.setValue(this.route.snapshot.params['id']);
      this.formProduto.controls.idCategoria.setValue('');
    }

    if (produto) {
      this.selectedProduto = produto;

      if (modo === 'E') {
        this.formProduto.patchValue(produto);
        this.formProduto.get('idCategoria').setValue(produto.idCategoria);
      }
    }
    
    this.modalService.open(modalId, { size: 'lg' });
  }

  closeModal() {
    this.modoCrud = null;
    this.selectedProduto = null;
    this.formProduto.reset();
    this.modalService.dismissAll();
  }

  saveProduto() {
    if (this.formProduto.invalid) {
      return;
    }

    if (this.modoCrud === 'N') {

      this.produtoService.insert(this.formProduto.value)
        .subscribe(response => {
          this.toaster.success(null, 'Novo produto adicionado na loja!');
          this.loadProdutosLoja();
          this.closeModal();
        },
        error => {});
    } else {
      this.produtoService.update(this.formProduto.value, this.selectedProduto)
        .subscribe(response => {
          this.toaster.success(null, 'Produto atualizado com sucesso!');
          this.loadProdutosLoja();
          this.closeModal();
        },
        error => {});
    }
  }

  excluirProduto() {
    this.produtoService.delete(this.selectedProduto.id.toString())
        .subscribe(response => {
          this.toaster.success(null, 'Produto removido com sucesso!');
          this.loadProdutosLoja();
          this.closeModal();
        },
        error => {
          this.closeModal();
        });
  }

}
