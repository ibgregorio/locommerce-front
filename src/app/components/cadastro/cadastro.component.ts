import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CidadeService } from 'app/service/cidade.service';
import { EstadoService } from 'app/service/estado.service';
import { UsuarioService } from 'app/service/usuario.service';
import { CategoriaService } from 'app/service/categoria.service';
import { CategoriaDTO } from 'models/categoria.dto';
import { CidadeDTO } from 'models/cidade.dto';
import { EstadoDTO } from 'models/estado.dto';
import { UsuarioNovoDTO } from 'models/usuario-novo.dto';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {

  formGroupDadosPessoais: FormGroup;
  formGroupTipo: FormGroup;
  formGroupLoja: FormGroup;
  formGroupEndereco: FormGroup;
  step: number = 1;

  usuarioNovo: UsuarioNovoDTO;

  estados: EstadoDTO[];
  cidades: CidadeDTO[];
  categorias: CategoriaDTO[];

  focusNome;
  focusCPF;
  focusEmail;
  focusTel;
  focusCel;
  focusSenha;
  focusConfirmSenha;

  focusNomeLoja;
  focusCNPJ;
  focusCatLoja;
  focusCepLoja;
  focusLogradouroLoja;
  focusNumLoja;
  focusComplementoLoja;
  focusBairroLoja;
  focusEstadoLoja;
  focusCidadeLoja;

  focusLogradouro;
  focusNum;
  focusComplemento;
  focusBairro;
  focusCep;
  focusEstado;
  focusCidade;

  constructor(
    public formBuilder: FormBuilder,
    public usuarioService: UsuarioService,
    public cidadeService: CidadeService,
    public estadoService: EstadoService,
    public categoriaService: CategoriaService,
    public router: Router,
    private toaster: ToastrService
  ) { 
    this.formGroupDadosPessoais = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(120)]],
      cpf: ['', [Validators.required, Validators.minLength(11)]],
      email: ['', [Validators.required, Validators.email]],
      numeTel: ['', [Validators.required]],
      numeCelular: ['', []],
      senha: ['', [Validators.required]],
      confirmaSenha: ['', [Validators.required]],
    });

    this.formGroupTipo = this.formBuilder.group({
      tipoUsuario: ['', [Validators.required]]
    });

    this.formGroupLoja = this.formBuilder.group({
      nomeFantasia: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(120)]],
      cnpj: ['', [Validators.required, Validators.minLength(14)]],
      idCategoria: ['', [Validators.required]],
      endereco: this.formBuilder.group({
        logradouro: ['', [Validators.required]],
        numero: ['', [Validators.required]],
        complemento: ['', []],
        bairro: ['', [Validators.required]],
        cep: ['', [Validators.required]],
        idEstado: ['', [Validators.required]],
        idCidade: ['', [Validators.required]]
      })
    });

    this.formGroupEndereco = this.formBuilder.group({
      logradouro: ['', [Validators.required]],
      numero: ['', [Validators.required]],
      complemento: ['', []],
      bairro: ['', [Validators.required]],
      cep: ['', [Validators.required]],
      idEstado: ['', [Validators.required]],
      idCidade: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');

    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.estadoService.findAll()
      .subscribe(response => {
        this.estados = response;
        this.formGroupEndereco.controls.idEstado.setValue('');
      },
      error => {});

    this.categoriaService.findAll()
      .subscribe(response => {
        this.categorias = response;
      },
      error => {});
  }

  ngOnDestroy(){
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');

    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }

  loadCidadesLoja() {
    let id_estado = this.formGroupLoja.get('endereco.idEstado').value; 

    this.cidadeService.findAll(id_estado)
      .subscribe(response => {
        this.cidades = response;
        this.formGroupLoja.patchValue({idCidade: ''});
      },
      error => {});
  }

  loadCidades() {
    let id_estado = this.formGroupEndereco.value.idEstado;

    if (id_estado === '') {
      this.cidades = [];
    }

    this.cidadeService.findAll(id_estado)
      .subscribe(response => {
        this.cidades = response;
        this.formGroupEndereco.controls.idCidade.setValue('');
      },
      error => {});
  }

  voltar() {
    if (this.step !== 2 && this.formGroupTipo.value.tipoUsuario) {
      this.step = 2;
    } else {
      this.step--;
    }
  }

  avancar() {
    if (this.step === 2 && this.formGroupTipo.value.tipoUsuario === '3') {
      this.step = 4;
    } else if (this.step === 2 && this.formGroupTipo.value.tipoUsuario === '2') {
      this.step = 3;
    } else {
      this.step++;
    }
  }

  concluirCadastro() {
    this.usuarioNovo = this.formGroupDadosPessoais.value;
    this.usuarioNovo.tipoUsuario = this.formGroupTipo.value.tipoUsuario;

    if (this.usuarioNovo.tipoUsuario.toString() === '2') {
      this.usuarioNovo.loja = this.formGroupLoja.value;
    } else {
      this.usuarioNovo.endereco = this.formGroupEndereco.value;
    }

    this.cadastrarUsuario(this.usuarioNovo);
  }

  cadastrarUsuario(obj: UsuarioNovoDTO) {
    this.usuarioService.insert(obj)
      .subscribe(response => {
        this.toaster.success(null, 'Cadastro concluído com sucesso!', {
          positionClass: 'toast-top-center'
        });
        this.router.navigate(['']);
      },
      error => {}); //usar o toaster para exibir mensagens do backend
  }

}
