import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CidadeService } from 'app/service/cidade.service';
import { EnderecoService } from 'app/service/endereco.service';
import { EstadoService } from 'app/service/estado.service';
import { UsuarioService } from 'app/service/usuario.service';
import { WebstorageService } from 'app/service/webstorage.service';
import { CidadeDTO } from 'models/cidade.dto';
import { EnderecoDTO } from 'models/endereco.dto';
import { EstadoDTO } from 'models/estado.dto';
import { UsuarioDTO } from 'models/usuario.dto';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {

  usuario: UsuarioDTO;
  selectedEndereco: EnderecoDTO;

  formDadosPessoais: FormGroup;
  formEndereco: FormGroup;

  estados: EstadoDTO[];
  cidades: CidadeDTO[];

  modoCrud: string;

  constructor(private modalService: NgbModal,
              private storage: WebstorageService,
              private usuarioService: UsuarioService,
              private cidadeService: CidadeService,
              private estadoService: EstadoService,
              private enderecoService: EnderecoService,
              private router: Router,
              private formBuilder: FormBuilder,
              private toaster: ToastrService) {

    this.formDadosPessoais = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(120)]],
      cpf: ['', [Validators.required, Validators.minLength(11)]],
      email: ['', [Validators.required, Validators.email]],
      numeTel: ['', [Validators.required]],
      numeCelular: ['', []]
    });

    this.formEndereco = this.formBuilder.group({
      logradouro: ['', [Validators.required]],
      numero: ['', [Validators.required]],
      complemento: ['', []],
      bairro: ['', [Validators.required]],
      cep: ['', [Validators.required]],
      idEstado: ['', [Validators.required]],
      idCidade: ['', [Validators.required]],
      principal: [null, [Validators.required]],
      idUsuario: [null, []]
    });
  }

  ngOnInit(): void {
    this.loadInfoUsuario();
  }

  loadInfoUsuario() {
    let localUser = this.storage.getLocalUser();

    if (localUser && localUser.email) {
      this.usuarioService.findByEmail(localUser.email)
        .subscribe(response => {
          this.usuario = response as UsuarioDTO;
        },
        error => {
          if (error.status == 403) {
            this.toaster.error('Você não tem permissão para utilizar esse recurso', 'Acesso não autorizado');
            this.router.navigate([`/login`]);
          }
        });
    } else {
      this.toaster.warning('Realize o login novamente', 'Sessão expirada');
      this.router.navigate([`/login`]);
    }
  }
  
  openModalDadosPessoais(modalId) {
    this.formDadosPessoais.patchValue(this.usuario);
    
    this.modalService.open(modalId, { size: 'lg' });
  }

  openModalEndereco(modalId, modo, endereco) {
    this.modoCrud = modo;

    if (modo === 'N' || modo === 'E') {
      this.loadCombosEndereco();
    }

    if (endereco) {
      this.selectedEndereco = endereco;

      if (modo === 'E') {
        this.formEndereco.patchValue(endereco);
        this.formEndereco.patchValue({
          idCidade: endereco.cidade.id.toString(),
          idEstado: endereco.cidade.estado.id.toString(),
          idUsuario: this.usuario.id,
          principal: endereco.principal.charAt(0)
        })
      }
    }
    
    this.modalService.open(modalId, { size: 'lg' });
  }

  saveDadosPessoais() {
    if (this.formDadosPessoais.invalid) {
      return;
    }

    this.usuarioService.update(this.formDadosPessoais.value, this.usuario)
      .subscribe(response => {
        this.toaster.success(null, 'Dados pessoais atualizados com sucesso!');
        this.loadInfoUsuario();
        this.modalService.dismissAll();
      },
      error => {});
  }

  saveEndereco() {
    if (this.formEndereco.invalid) {
      return;
    }

    if (this.modoCrud === 'N') {
      this.formEndereco.patchValue({ idUsuario: this.usuario.id })

      this.enderecoService.insert(this.formEndereco.value)
        .subscribe(response => {
          this.toaster.success(null, 'Novo endereço adicionado!');
          this.loadInfoUsuario();
          this.closeModal();
        },
        error => {});
    } else {
      this.enderecoService.update(this.formEndereco.value, this.selectedEndereco)
        .subscribe(response => {
          this.toaster.success(null, 'Endereço atualizado com sucesso!');
          this.loadInfoUsuario();
          this.closeModal();
        },
        error => {});
    }
  }

  excluirEndereco() {
    this.enderecoService.delete(this.selectedEndereco.id.toString())
        .subscribe(response => {
          this.toaster.success(null, 'Endereço removido com sucesso!');
          this.loadInfoUsuario();
          this.closeModal();
        },
        error => {
          this.closeModal();
        });
  }

  private loadCombosEndereco() {
    this.estadoService.findAll()
      .subscribe(response => {
        this.estados = response;
        this.formEndereco.controls.idEstado.setValue('');
      },
      error => {});
  }

  loadCidades() {
    let id_estado = this.formEndereco.value.idEstado;

    if (id_estado === '') {
      this.cidades = [];
    }

    this.cidadeService.findAll(id_estado)
      .subscribe(response => {
        this.cidades = response;
        this.formEndereco.controls.idCidade.setValue('');
      },
      error => {});
  }

  closeModal() {
    this.modoCrud = null;
    this.selectedEndereco = null;
    this.formDadosPessoais.reset();
    this.formEndereco.reset();
    this.modalService.dismissAll();

    this.estados = [];
    this.cidades = [];
  }
}
