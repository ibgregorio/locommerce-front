import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CarrinhoService } from 'app/service/carrinho.service';
import { ItemDesejoService } from 'app/service/item-desejo.service';
import { ListDesejosService } from 'app/service/list-desejos.service';
import { ProdutoService } from 'app/service/produto.service';
import { ItemNovoDesejoDTO } from 'models/item-novo-desejo.dto';
import { ListaDesejoDTO } from 'models/lista-desejo.dto';
import { ProdutoDTO } from 'models/produto.dto';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.scss']
})
export class ProdutoComponent implements OnInit {

  produto: ProdutoDTO;
  quantidade: number = 1;

  listasDesejos: ListaDesejoDTO[];
  selectedLista: ListaDesejoDTO;

  constructor(public produtoService: ProdutoService, 
              public route: ActivatedRoute,
              public router: Router,
              public carrinhoService: CarrinhoService,
              private modalService: NgbModal,
              private listDesejosService: ListDesejosService,
              private itemDesejoService: ItemDesejoService,
              private toaster: ToastrService) { }

  ngOnInit(): void {
    let id_produto = this.route.snapshot.params['id'];

    this.produtoService.findById(id_produto)
      .subscribe(response => {
        this.produto = response;
      },
      error => {});
  }

  comprarProduto(produto: ProdutoDTO) {
    this.carrinhoService.adicionarProduto(produto, this.quantidade);
    this.router.navigate([`/order`]);
  }

  adicionarAoCarrinho(produto: ProdutoDTO) {
    this.carrinhoService.adicionarProduto(produto, this.quantidade);
    this.router.navigate([`/cart`]);
  }

  openModal(modalId) {   
    this.listDesejosService.findByUsuarioLogado()
        .subscribe(response => {
          this.listasDesejos = response;      
        },
        error => {});
    
    this.modalService.open(modalId, { size: 'lg' });
  }

  closeModal() {
    this.selectedLista = null;
    this.modalService.dismissAll();
  }

  adicionarItemLista(item: ListaDesejoDTO) {

    let itemAdd: ItemNovoDesejoDTO = {
      idLista: item.id,
      idProduto: this.produto.id
    }

    this.itemDesejoService.adicionarItem(itemAdd)
        .subscribe(response => {
          this.toaster.success(null, 'Produto adicionado à lista de desejos');
          this.closeModal();
        },
        error => {});
  }

}
