import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/service/auth.service';
import { LoginDTO } from 'models/login.dto';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    login: LoginDTO = {
      email: "",
      senha: ""
    };

    focusEmail;
    focusSenha;

    constructor(public auth: AuthService, public router: Router) { }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');

        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
    }

    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');

        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
    }

    realizarLogin() {
      this.auth.authenticate(this.login)
        .subscribe(response => {
          this.auth.successfulLogin(response.headers.get('Authorization'), JSON.parse(response.body));
          this.router.navigate(['/']);
        },
        error => {});
    }

    cadastrar() {
      this.router.navigate(['/signup']);
    }

}
