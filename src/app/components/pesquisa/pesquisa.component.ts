import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pesquisa',
  templateUrl: './pesquisa.component.html',
  styleUrls: ['./pesquisa.component.scss']
})
export class PesquisaComponent implements OnInit {

  tipoBusca: string = this.route.snapshot.queryParamMap.get('opcao');

  constructor(public route: ActivatedRoute) { }

  ngOnInit(): void {
  }
}
