import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AvaliacaoService } from 'app/service/avaliacao.service';
import { LojaService } from 'app/service/loja.service';
import { PedidoService } from 'app/service/pedido.service';
import { UsuarioService } from 'app/service/usuario.service';
import { WebstorageService } from 'app/service/webstorage.service';
import { AvaliacaoDTO } from 'models/avaliacao.dto';
import { LojaDTO } from 'models/loja.dto';
import { PedidoDetail } from 'models/pedido-detail';
import { UsuarioDTO } from 'models/usuario.dto';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-pedidos-usuario',
  templateUrl: './pedidos-usuario.component.html',
  styleUrls: ['./pedidos-usuario.component.scss']
})
export class PedidosUsuarioComponent implements OnInit {

  pedidos: PedidoDetail[];
  avaliacao: AvaliacaoDTO;
  selectedNota: number = 0;

  constructor(private modalService: NgbModal,
              private pedidoService: PedidoService,
              private usuarioService: UsuarioService,
              private avaliacaoService: AvaliacaoService,
              private lojaService: LojaService,
              private storage: WebstorageService,
              private toaster: ToastrService,
              private router: Router) { }

  ngOnInit(): void {
    this.loadPedidos();
  }
  
  loadPedidos() {
    this.pedidoService.findByUsuarioLogado()
        .subscribe(response => {
          this.pedidos = response;
        },
        error => {});
  }

  viewDetails(pedido: PedidoDetail) {
    this.router.navigate([`/order/${pedido.id}`]);
  }

  saveAvaliacao() { 
    this.avaliacao.nota = this.selectedNota;

    this.avaliacaoService.insert(this.avaliacao)
      .subscribe(response => {
        this.toaster.success(null, 'A avaliação do vendedor escolhido foi publicada');
        this.closeModal();
      },
      error => {});
  }

  openModal(modalId, produto) {
    let usuario : UsuarioDTO;
    let lojaSelecionada: LojaDTO;
    let localUser = this.storage.getLocalUser();
    
    this.usuarioService.findByEmail(localUser.email)
    .subscribe(response => {
      usuario = response as UsuarioDTO;
      
      this.avaliacao = {
        nota: 0,
        idLojaAvaliada: null,
        idUsuarioAvaliacao: usuario.id
      };
    },
    error => {});

    this.lojaService.findLojaByProduto(produto.id)
    .subscribe(response => {
      lojaSelecionada = response as LojaDTO;
      this.avaliacao.idLojaAvaliada = lojaSelecionada.id;
    },
    error => {});
    
    this.modalService.open(modalId, { size: 'lg' });
  }

  closeModal() {
    this.avaliacao = null;
    this.selectedNota = 0;
    this.modalService.dismissAll();
  }

}
