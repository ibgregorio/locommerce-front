import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarrinhoService } from 'app/service/carrinho.service';
import { LojaService } from 'app/service/loja.service';
import { ProdutoService } from 'app/service/produto.service';
import { LojaDTO } from 'models/loja.dto';
import { ProdutoDTO } from 'models/produto.dto';

@Component({
  selector: 'app-loja',
  templateUrl: './loja.component.html',
  styleUrls: ['./loja.component.scss']
})
export class LojaComponent implements OnInit {

  loja: LojaDTO;
  itens: ProdutoDTO[];

  lojaUsuarioLogado: boolean = false;

  constructor(public lojaService: LojaService, 
              public router: Router, 
              public route: ActivatedRoute,
              private produtoService: ProdutoService,
              private carrinhoService: CarrinhoService) { }

  ngOnInit(): void {
    let id_loja = this.route.snapshot.params['id'];

    this.lojaService.findById(id_loja)
      .subscribe(response => {
        this.loja = response;
      },
      error => {});

      this.produtoService.loadProdutosFromLoja(id_loja)
        .subscribe(response => {
          this.itens = response;          
        },
        error => {});
  }

  verProdutos(loja: LojaDTO) {
    this.router.navigate([`/store/${loja.id}/products`]);
  }

  comprarProduto(produto: ProdutoDTO) {
    this.carrinhoService.adicionarProduto(produto, 1);
    this.router.navigate([`/order`]);
  }

  visualizar(produto: ProdutoDTO) {
    this.router.navigate([`/product/${produto.id}`]);
  }
}
