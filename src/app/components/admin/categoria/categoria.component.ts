import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoriaService } from 'app/service/categoria.service';
import { CategoriaDTO } from 'models/categoria.dto';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss']
})
export class CategoriaComponent implements OnInit {

  selectedCategoria: CategoriaDTO;
  categorias: CategoriaDTO[];
  formCategoria: FormGroup;

  modoCrud: string;

  constructor(private modalService: NgbModal,
              private categoriaService: CategoriaService,
              private formBuilder: FormBuilder,
              private toaster: ToastrService,
              private router: Router) { 
    this.formCategoria = this.formBuilder.group({
      descricao: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
    });
  }

  ngOnInit(): void {
    this.loadCategorias();
  }
  
  loadCategorias() {
    this.categoriaService.findAll()
            .subscribe(response => {
                this.categorias = response;
            },
            error => {});
  }

  openModal(modalId, modo, categoria) {   
    this.modoCrud = modo;

    if (categoria) {
      this.selectedCategoria = categoria;

      if (modo === 'E') {
        this.formCategoria.patchValue(categoria);
      }
    }
    
    this.modalService.open(modalId, { size: 'lg' });
  }

  closeModal() {
    this.modoCrud = null;
    this.selectedCategoria = null;
    this.formCategoria.reset();
    this.modalService.dismissAll();
  }

  saveCategoria() {
    if (this.formCategoria.invalid) {
      return;
    }

    if (this.modoCrud === 'N') {

      this.categoriaService.insert(this.formCategoria.value)
        .subscribe(response => {
          this.toaster.success(null, 'Nova categoria adicionada!');
          this.loadCategorias();
          this.closeModal();
        },
        error => {});
    } else {
      this.categoriaService.update(this.formCategoria.value, this.selectedCategoria)
        .subscribe(response => {
          this.toaster.success(null, 'Categoria atualizada com sucesso!');
          this.loadCategorias();
          this.closeModal();
        },
        error => {});
    }
  }

  excluirCategoria() {
    this.categoriaService.delete(this.selectedCategoria.id.toString())
        .subscribe(response => {
          this.toaster.success(null, 'Categoria removida com sucesso!');
          this.loadCategorias();
          this.closeModal();
        },
        error => {
          this.closeModal();
        });
  }

  retornarParaDashboard() {
    this.router.navigate([`/admin/dashboard`]);
  }
}
