import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UsuarioService } from 'app/service/usuario.service';
import { WebstorageService } from 'app/service/webstorage.service';
import { UsuarioDTO } from 'models/usuario.dto';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {

  usuarios: UsuarioDTO[];
  formUsuario: FormGroup;

  adminLogado: string;

  selectedUsuario: UsuarioDTO;
  modoCrud: string;

  constructor(private modalService: NgbModal,
              private router: Router,
              private usuarioService: UsuarioService,
              private formBuilder: FormBuilder,
              private toaster: ToastrService,
              private storage: WebstorageService) {

    this.formUsuario = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(120)]],
      cpf: ['', [Validators.required, Validators.minLength(11)]],
      email: ['', [Validators.required, Validators.email]],
      numeTel: ['', [Validators.required]],
      numeCelular: ['', []]
    });
  }

  ngOnInit(): void {
    this.loadUsuarios();
    this.adminLogado = this.storage.getLocalUser().email;
  }
  
  loadUsuarios() {
    this.usuarioService.findAll()
        .subscribe(response => {
          this.usuarios = response;      
        },
        error => {});
  }

  openModal(modalId, modo, usuario) {
    this.modoCrud = modo;

    if (usuario) {
      this.selectedUsuario = usuario;
      this.formUsuario.patchValue(this.selectedUsuario)
    }

    if (this.modoCrud === 'V') {
      this.disableFormFields();
    }

    this.modalService.open(modalId, { size: 'md', scrollable: true });
  }

  closeModal() {
    this.modoCrud = null;
    this.selectedUsuario = null;
    this.formUsuario.reset();
    this.formUsuario.enable();
    this.modalService.dismissAll();
  }

  atualizar() {
    if (this.formUsuario.invalid) {
      return;
    }

    this.usuarioService.update(this.formUsuario.value, this.selectedUsuario)
      .subscribe(response => {
        this.toaster.success(null, 'As informações do usuário foram atualizadas!');
        this.loadUsuarios();
        this.closeModal();
      },
      error => {});
  }

  desativarUsuario() {
    this.usuarioService.desativar(this.selectedUsuario.id.toString())
        .subscribe(response => {
          this.toaster.success(null, 'O usuário foi desativado com sucesso');
          this.loadUsuarios();
          this.closeModal();
        },
        error => {
          this.closeModal();
        });
  }

  private disableFormFields() {
    this.formUsuario.controls['nome'].disable();
    this.formUsuario.controls['cpf'].disable();
    this.formUsuario.controls['email'].disable();
    this.formUsuario.controls['numeTel'].disable();
    this.formUsuario.controls['numeCelular'].disable();
  }

  retornarParaDashboard() {
    this.router.navigate([`/admin/dashboard`]);
  }

}
