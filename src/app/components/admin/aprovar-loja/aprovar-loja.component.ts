import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LojaService } from 'app/service/loja.service';
import { LojaDTO } from 'models/loja.dto';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aprovar-loja',
  templateUrl: './aprovar-loja.component.html',
  styleUrls: ['./aprovar-loja.component.scss']
})
export class AprovarLojaComponent implements OnInit {

  aprovacao: boolean;
  selectedLoja: LojaDTO;
  lojas: LojaDTO[];

  formJustificativa: FormGroup;

  constructor(private modalService: NgbModal,
              private lojaService: LojaService,
              private formBuilder: FormBuilder,
              private toaster: ToastrService,
              private router: Router) {
      this.formJustificativa = this.formBuilder.group({
        justificativaReprovacao: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      });
  }

  ngOnInit(): void {
    this.loadLojasPendentes();
  }

  loadLojasPendentes() {
    this.lojaService.findLojasNaoAprovadas()
            .subscribe(response => {
                this.lojas = response;
            },
            error => {});
  }

  openModal(modalId, tipo, loja) {
    this.loadDadosLoja(loja);

    if (tipo === 'A') {
      this.aprovacao = true;
    } else {
      this.aprovacao = false;
    }
    
    this.modalService.open(modalId, { size: 'lg' });
  }

  confirmarOperacao(modalReprovacao) {
    
    if (this.aprovacao) {
      this.aprovarLoja();
    } else {
      this.modalService.dismissAll();
      this.modalService.open(modalReprovacao, { size: 'lg' });
    }
    
  }

  aprovarLoja() {
    this.lojaService.aprovarLoja(this.selectedLoja)
        .subscribe(response => {
          this.toaster.success('Um e-mail foi enviado ao proprietário', 'Loja aprovada');
          this.loadLojasPendentes();
          this.closeModal();
        },
        error => {});
    
    this.modalService.dismissAll();
  }

  reprovarLoja() {
    this.selectedLoja.justificativaReprovacao = this.formJustificativa.controls['justificativaReprovacao'].value;
    this.selectedLoja.idCategoria = this.selectedLoja.categoria.id.toString();
    this.selectedLoja.situacao = 'I';

    this.lojaService.reprovarLoja(this.selectedLoja)
        .subscribe(response => {
          this.toaster.success('Um e-mail foi enviado ao proprietário com o motivo da reprovação', 'Loja reprovada');
          this.loadLojasPendentes();
          this.closeModal();
        },
        error => {});
    
    this.modalService.dismissAll();
  }

  closeModal() {
    this.selectedLoja = null;
    this.formJustificativa.reset();
    this.modalService.dismissAll();
  }

  private loadDadosLoja(loja: LojaDTO) {
    this.lojaService.findById(loja.id.toString())
      .subscribe(response => {
        this.selectedLoja = response;
      },
      error => {});
  }

  retornarParaDashboard() {
    this.router.navigate([`/admin/dashboard`]);
  }

}
