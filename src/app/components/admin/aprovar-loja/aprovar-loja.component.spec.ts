import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprovarLojaComponent } from './aprovar-loja.component';

describe('AprovarLojaComponent', () => {
  let component: AprovarLojaComponent;
  let fixture: ComponentFixture<AprovarLojaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprovarLojaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprovarLojaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
