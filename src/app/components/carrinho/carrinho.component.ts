import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CarrinhoService } from 'app/service/carrinho.service';
import { ProdutoService } from 'app/service/produto.service';
import { ItemCarrinho } from 'models/item-carrinho';
import { ProdutoDTO } from 'models/produto.dto';

@Component({
  selector: 'app-carrinho',
  templateUrl: './carrinho.component.html',
  styleUrls: ['./carrinho.component.scss']
})
export class CarrinhoComponent implements OnInit {

  items: ItemCarrinho[];

  constructor(
    public carrinhoService: CarrinhoService,
    public router: Router,
    public produtoService: ProdutoService) { }

  ngOnInit(): void {
    let carrinho = this.carrinhoService.getCarrinho();

    this.items = carrinho.itens;
  }

  total() : number {
    return this.carrinhoService.total();
  }

  removerItem(produto: ProdutoDTO) {
    this.items = this.carrinhoService.removerProduto(produto).itens;
  }

  aumentarQtd(produto: ProdutoDTO) {
    this.items = this.carrinhoService.aumentarQtd(produto).itens;
  }

  diminuirQtd(produto: ProdutoDTO) {
    this.items = this.carrinhoService.diminuirQtd(produto).itens;
  }

  retornarParaHome() {
    this.router.navigate([`/`]);
  }

  fecharPedido() {
    this.router.navigate(['/order']);
  }
}
