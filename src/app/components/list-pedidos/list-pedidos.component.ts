import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PedidoService } from 'app/service/pedido.service';
import { PedidoDetail } from 'models/pedido-detail';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-pedidos',
  templateUrl: './list-pedidos.component.html',
  styleUrls: ['./list-pedidos.component.scss']
})
export class ListPedidosComponent implements OnInit {

  pedidos: PedidoDetail[];
  opcoesStatus = []

  selectedPedido: PedidoDetail;
  modoCrud: string;

  formStatus: FormGroup;

  constructor(private modalService: NgbModal,
              private router: Router,
              private pedidoService: PedidoService,
              private formBuilder: FormBuilder,
              private toaster: ToastrService) {

    this.formStatus = this.formBuilder.group({
      status: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.loadPedidos();
  }
  
  loadPedidos() {
    this.pedidoService.findByLoja()
        .subscribe(response => {
          this.pedidos = response;          
        },
        error => {});
  }

  viewDetails(pedido: PedidoDetail) {
    this.router.navigate([`/order/${pedido.id}`]);
  }

  openModal(modalId, modo, pedido) {
    this.modoCrud = modo;

    if (pedido) {
      this.selectedPedido = pedido;
      this.formStatus.controls.status.setValue(this.selectedPedido.status.substring(0,1));
    }

    this.modalService.open(modalId, { size: 'md', scrollable: true });
  }

  closeModal() {
    this.modoCrud = null;
    this.selectedPedido = null;
    this.formStatus.reset();
    this.modalService.dismissAll();
  }

  atualizarStatus() {
    if (this.formStatus.invalid) {
      return;
    }

    this.pedidoService.update(this.formStatus.value, this.selectedPedido)
      .subscribe(response => {
        this.toaster.success(null, 'Status do pedido foi atualizado!');
        this.loadPedidos();
        this.closeModal();
      },
      error => {});
  }

  cancelarPedido() {
    this.pedidoService.cancelar(this.selectedPedido.id.toString())
        .subscribe(response => {
          this.toaster.success(null, 'Pedido cancelado com sucesso!');
          this.loadPedidos();
          this.closeModal();
        },
        error => {
          this.closeModal();
        });
  }

}
