import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoriaService } from 'app/service/categoria.service';
import { CategoriaDTO } from 'models/categoria.dto';

@Component({
    selector: 'app-components',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit, OnDestroy {

    categorias: CategoriaDTO[];
    searchForm: FormGroup;
    focusSearch;

    constructor(public categoriaService: CategoriaService, 
                public router: Router,
                public formBuilder: FormBuilder) {
        
        this.searchForm = this.formBuilder.group({
            searchContent: ['', []],
            tipoBusca: ['P', []]
        });
    }

    ngOnInit() {
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');

        this.categoriaService.findAll()
            .subscribe(response => {
                this.categorias = response;
            },
            error => {});
    }

    ngOnDestroy(){
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
    }

    searchByName() {
        let { searchContent, tipoBusca } = this.searchForm.value;
        
        this.router.navigate(['/search'], {queryParams: {opcao: tipoBusca, nome: searchContent}});
    }

    searchSelectedCategoria(id_categoria: number) {
        let { tipoBusca } = this.searchForm.value;

        this.router.navigate(['/search'], {queryParams: {opcao: tipoBusca, categoria: id_categoria}});
    }
}
