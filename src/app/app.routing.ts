import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { PesquisaComponent } from './components/pesquisa/pesquisa.component';
import { ProdutoComponent } from './components/produto/produto.component';
import { LojaComponent } from './components/loja/loja.component';
import { LojaProdutoComponent } from './components/loja-produto/loja-produto.component';
import { LoginComponent } from './components/login/login.component';
import { CadastroComponent } from './components/cadastro/cadastro.component';
import { CarrinhoComponent } from './components/carrinho/carrinho.component';
import { PedidoComponent } from './components/pedido/pedido.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { AuthGuard } from './guards/auth.guard';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { CategoriaComponent } from './components/admin/categoria/categoria.component';
import { AprovarLojaComponent } from './components/admin/aprovar-loja/aprovar-loja.component';
import { ListPedidosComponent } from './components/list-pedidos/list-pedidos.component';
import { PedidoDetailComponent } from './components/pedido-detail/pedido-detail.component';
import { UsuarioComponent } from './components/admin/usuario/usuario.component';
import { ListDesejosComponent } from './components/list-desejos/list-desejos.component';
import { PedidosUsuarioComponent } from './components/pedidos-usuario/pedidos-usuario.component';
import { RelatorioLojaComponent } from './components/relatorio-loja/relatorio-loja.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home',  component: HomeComponent },
    { path: 'login',  component: LoginComponent },
    { path: 'signup',  component: CadastroComponent },
    { path: 'search',  component: PesquisaComponent },
    { path: 'product/:id',  component: ProdutoComponent },
    { path: 'store/:id',  component: LojaComponent },
    { path: 'store/:id/products',  component: LojaProdutoComponent },
    { path: 'cart',  component: CarrinhoComponent },
    { path: 'order',  component: PedidoComponent, canActivate: [AuthGuard] },
    { path: 'profile',  component: PerfilComponent, canActivate: [AuthGuard] },
    { path: 'admin/dashboard',  component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'admin/categories',  component: CategoriaComponent, canActivate: [AuthGuard] },
    { path: 'admin/stores',  component: AprovarLojaComponent, canActivate: [AuthGuard] },
    { path: 'admin/users',  component: UsuarioComponent, canActivate: [AuthGuard] },
    { path: 'store/:id/orders',  component: ListPedidosComponent, canActivate: [AuthGuard] },
    { path: 'order/:id',  component: PedidoDetailComponent, canActivate: [AuthGuard] },
    { path: 'wishlists',  component: ListDesejosComponent, canActivate: [AuthGuard] },
    { path: 'myorders',  component: PedidosUsuarioComponent, canActivate: [AuthGuard] },
    { path: 'stats',  component: RelatorioLojaComponent, canActivate: [AuthGuard] }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
