import { Component, OnInit, ElementRef } from '@angular/core';
import { AuthService } from 'app/service/auth.service';
import { Router } from '@angular/router';
import { UsuarioService } from 'app/service/usuario.service';
import { UsuarioDTO } from 'models/usuario.dto';
import { WebstorageService } from 'app/service/webstorage.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    private toggleButton: any;
    private sidebarVisible: boolean;

    constructor(private element : ElementRef, 
                private router: Router, 
                public authService: AuthService,
                private usuarioService: UsuarioService,
                private storage: WebstorageService) {
        this.sidebarVisible = false;
    }

    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const html = document.getElementsByTagName('html')[0];
        setTimeout(function(){
            toggleButton.classList.add('toggled');
        }, 500);
        html.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    };
    sidebarToggle() {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };
  
    sair() {
        this.authService.logout();
        this.router.navigate([`/login`]);
    }

    showProdutosVendedor() {
        let usuarioLogado: UsuarioDTO;
        let localUser = this.storage.getLocalUser();

        this.usuarioService.findByEmail(localUser.email)
            .subscribe(response => {
                usuarioLogado = response as UsuarioDTO;
                this.router.navigate([`/store/${usuarioLogado.loja.id}/products`]);
            },
            error => {});
    }

    showPedidosVendedor() {
        let usuarioLogado: UsuarioDTO;
        let localUser = this.storage.getLocalUser();

        this.usuarioService.findByEmail(localUser.email)
            .subscribe(response => {
                usuarioLogado = response as UsuarioDTO;
                this.router.navigate([`/store/${usuarioLogado.loja.id}/orders`]);
            },
            error => {});
    }
}
