import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { ToastrModule } from 'ngx-toastr';
import { HomeModule } from './components/home/home.module';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { CategoriaService } from './service/categoria.service';
import { HttpClientModule } from '@angular/common/http';
import { PesquisaComponent } from './components/pesquisa/pesquisa.component';
import { ProdutoComponent } from './components/produto/produto.component';
import { PesquisaProdutoComponent } from './components/pesquisa-produto/pesquisa-produto.component';
import { PesquisaLojaComponent } from './components/pesquisa-loja/pesquisa-loja.component';
import { LojaComponent } from './components/loja/loja.component';
import { LojaProdutoComponent } from './components/loja-produto/loja-produto.component';
import { LoginComponent } from './components/login/login.component';
import { AuthService } from './service/auth.service';
import { WebstorageService } from './service/webstorage.service';
import { CadastroComponent } from './components/cadastro/cadastro.component';
import { ErrorInterceptorProvider } from 'interceptors/error-interceptor';
import { PedidoComponent } from './components/pedido/pedido.component';
import { CarrinhoComponent } from './components/carrinho/carrinho.component';
import { AuthInterceptorProvider } from 'interceptors/auth-interceptor';
import { PerfilComponent } from './components/perfil/perfil.component';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { CategoriaComponent } from './components/admin/categoria/categoria.component';
import { AprovarLojaComponent } from './components/admin/aprovar-loja/aprovar-loja.component';
import { PedidoDetailComponent } from './components/pedido-detail/pedido-detail.component';
import { ListPedidosComponent } from './components/list-pedidos/list-pedidos.component';
import { UsuarioComponent } from './components/admin/usuario/usuario.component';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { ListDesejosComponent } from './components/list-desejos/list-desejos.component';
import { PedidosUsuarioComponent } from './components/pedidos-usuario/pedidos-usuario.component';
import { RelatorioLojaComponent } from './components/relatorio-loja/relatorio-loja.component';
import { ChartsModule } from 'ng2-charts';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        PesquisaComponent,
        ProdutoComponent,
        PesquisaProdutoComponent,
        PesquisaLojaComponent,
        LojaComponent,
        LojaProdutoComponent,
        LoginComponent,
        CadastroComponent,
        CarrinhoComponent,
        PedidoComponent,
        PerfilComponent,
        DashboardComponent,
        CategoriaComponent,
        AprovarLojaComponent,
        ListPedidosComponent,
        PedidoDetailComponent,
        UsuarioComponent,
        ListDesejosComponent,
        PedidosUsuarioComponent,
        RelatorioLojaComponent
    ],
    imports: [
        BrowserAnimationsModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        AppRoutingModule,
        HomeModule,
        HttpClientModule,
        ToastrModule.forRoot({
            positionClass: 'toast-top-center',
            preventDuplicates: true
        }),
        NgxMaskModule.forRoot(),
        ChartsModule
    ],
    providers: [CategoriaService, AuthService, WebstorageService, ErrorInterceptorProvider, AuthInterceptorProvider],
    bootstrap: [AppComponent]
})
export class AppModule { }
