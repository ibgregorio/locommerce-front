import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { EnderecoDTO } from 'models/endereco.dto';

@Injectable({
  providedIn: 'root'
})
export class EnderecoService {

  constructor(public http: HttpClient) { }

  findById(id: string) {
    return this.http.get(`${environment.baseUrl}/enderecos/${id}`);
  }

  insert(obj : EnderecoDTO) {
      return this.http.post(
          `${environment.baseUrl}/enderecos`,
          obj,
          {
              observe: 'response',
              responseType: 'text'
          }
      );
  }

  update(obj : EnderecoDTO, endereco: EnderecoDTO) {
    return this.http.put(
        `${environment.baseUrl}/enderecos/${endereco.id}`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

  delete(id: string) {
    return this.http.delete(
        `${environment.baseUrl}/enderecos/${id}`,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 
}
