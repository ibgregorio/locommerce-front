import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { PedidoDetail } from 'models/pedido-detail';
import { PedidoDTO } from 'models/pedido.dto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PedidoService {

  constructor(public http: HttpClient) {
  }

  findById(id: string) {
    return this.http.get(`${environment.baseUrl}/pedidos/${id}`);
  }
  
  findByUsuarioLogado() : Observable<PedidoDetail[]>{
    return this.http.get<PedidoDetail[]>(`${environment.baseUrl}/pedidos`);
  }

  findByLoja() : Observable<PedidoDetail[]>{
    return this.http.get<PedidoDetail[]>(`${environment.baseUrl}/pedidos/loja`);
  }

  insert(obj : PedidoDTO) {
      return this.http.post(
          `${environment.baseUrl}/pedidos`,
          obj,
          {
              observe: 'response',
              responseType: 'text'
          }
      );
  }

  update(obj : any, selected: PedidoDetail) {
    return this.http.put(
        `${environment.baseUrl}/pedidos/${selected.id}`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

  cancelar(id: string) {
    return this.http.delete(
        `${environment.baseUrl}/pedidos/${id}`,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 
}
