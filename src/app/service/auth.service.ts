import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { AuthResponse } from 'models/authresponse';
import { LoginDTO } from 'models/login.dto';
import { StoredUser } from 'models/stored_user';
import { WebstorageService } from './webstorage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public http: HttpClient, public storage: WebstorageService) {    
  }

  authenticate(login : LoginDTO) {
      return this.http.post(
        `${environment.baseUrl}/auth/login`,
        login,
        {
          observe: 'response',
          responseType: 'text'
        });
  }

  refreshToken() {
      return this.http.post(
        `${environment.baseUrl}/auth/refresh_token`,
        {},
        {
          observe: 'response',
          responseType: 'text'
        });
  }

  successfulLogin(authorizationValue : string, authResponse: AuthResponse) {
      let tok = authorizationValue.substring(7);
      let user : StoredUser = {
          token: tok,
          email: authResponse.email,
          perfil: authResponse.perfil
      };

      this.storage.setLocalUser(user);
  }

  isAuthenticated(): boolean {
    if (this.storage.getLocalUser()){
      return true;
    } else{
      return false;
    }
  }

  isAdmin(): boolean {
    return this.isAuthenticated() && this.storage.getLocalUser().perfil == 'ROLE_ADMIN';
  }

  isVendedor(): boolean {
    return this.isAuthenticated() && this.storage.getLocalUser().perfil == 'ROLE_VENDEDOR';
  }

  logout() {
      this.storage.setLocalUser(null);
  }
}
