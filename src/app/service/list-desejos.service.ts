import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { ListaDesejoDTO } from 'models/lista-desejo.dto';
import { Observable } from 'rxjs';
import { WebstorageService } from './webstorage.service';

@Injectable({
  providedIn: 'root'
})
export class ListDesejosService {

  constructor(
    public http: HttpClient, 
    public webstorage: WebstorageService) {}

  findById(id: string) {
    return this.http.get(`${environment.baseUrl}/listaDesejos/${id}`);
  }

  findByUsuarioLogado() : Observable<ListaDesejoDTO[]> {
    return this.http.get<ListaDesejoDTO[]>(`${environment.baseUrl}/listaDesejos`);
  }

  insert(obj : ListaDesejoDTO) {
      return this.http.post(
          `${environment.baseUrl}/listaDesejos`,
          obj,
          {
              observe: 'response',
              responseType: 'text'
          }
      );
  }

  update(obj : ListaDesejoDTO, lista: ListaDesejoDTO) {
    return this.http.put(
        `${environment.baseUrl}/listaDesejos/${lista.id}`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

  delete(id: string) {
    return this.http.delete(
        `${environment.baseUrl}/listaDesejos/${id}`,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 
}
