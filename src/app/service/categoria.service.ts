import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CategoriaDTO } from 'models/categoria.dto';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CategoriaService {

  constructor(public http: HttpClient) { }

  findAll() : Observable<CategoriaDTO[]> {
    return this.http.get<CategoriaDTO[]>(`${environment.baseUrl}/categorias`);
  }

  insert(obj : CategoriaDTO) {
    return this.http.post(
        `${environment.baseUrl}/categorias`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  }

  update(obj : CategoriaDTO, selected: CategoriaDTO) {
    return this.http.put(
        `${environment.baseUrl}/categorias/${selected.id}`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

  delete(id: string) {
    return this.http.delete(
        `${environment.baseUrl}/categorias/${id}`,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 
}
