import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { ItemNovoDesejoDTO } from 'models/item-novo-desejo.dto';

@Injectable({
  providedIn: 'root'
})
export class ItemDesejoService {

  constructor(public http: HttpClient) { }

  adicionarItem(obj : ItemNovoDesejoDTO) {
    return this.http.post(
        `${environment.baseUrl}/listaDesejos/itens`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  }

  delete(idLista: string, idProduto: string) {
    return this.http.delete(
        `${environment.baseUrl}/listaDesejos/itens/${idProduto}/selectedList/${idLista}`,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 
}
