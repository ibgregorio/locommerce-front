import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WebstorageService } from './webstorage.service';
import { UsuarioDTO } from 'models/usuario.dto';
import { UsuarioNovoDTO } from 'models/usuario-novo.dto';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    public http: HttpClient, 
    public webstorage: WebstorageService) {
}

  findById(id: string) {
    return this.http.get(`${environment.baseUrl}/usuarios/${id}`);
  }

  findAll() : Observable<UsuarioDTO[]> {
    return this.http.get<UsuarioDTO[]>(`${environment.baseUrl}/usuarios`);
  }

  findByEmail(email: string) {
      return this.http.get(`${environment.baseUrl}/usuarios/email?value=${email}`);
  }

  insert(obj : UsuarioNovoDTO) {
      return this.http.post(
          `${environment.baseUrl}/usuarios`,
          obj,
          {
              observe: 'response',
              responseType: 'text'
          }
      );
  }

  update(obj : UsuarioDTO, usuario: UsuarioDTO) {
    return this.http.put(
        `${environment.baseUrl}/usuarios/${usuario.id}`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

  desativar(id: string) {
    return this.http.delete(
        `${environment.baseUrl}/usuarios/${id}`,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

}
