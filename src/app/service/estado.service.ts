import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { EstadoDTO } from 'models/estado.dto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  constructor(public http: HttpClient) {
  }

  findAll() : Observable<EstadoDTO[]> {
      return this.http.get<EstadoDTO[]>(`${environment.baseUrl}/estados`);
  }
}
