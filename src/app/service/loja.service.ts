import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LojaDTO } from 'models/loja.dto';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LojaService {

  constructor(public http: HttpClient) { }

  findLoja(id_categoria : string, nome_loja: string) : Observable<LojaDTO[]>{
    if (id_categoria && nome_loja) {
      return this.http.get<LojaDTO[]>(`${environment.baseUrl}/lojas/?categoria=${id_categoria}&nome=${nome_loja}`);
    } else if (id_categoria) {
      return this.http.get<LojaDTO[]>(`${environment.baseUrl}/lojas/?categoria=${id_categoria}`);
    } else if (nome_loja) {
      return this.http.get<LojaDTO[]>(`${environment.baseUrl}/lojas/?nome=${nome_loja}`);
    } else {
      return this.http.get<LojaDTO[]>(`${environment.baseUrl}/lojas`);
    }
  }

  findLojasNaoAprovadas() : Observable<LojaDTO[]>{
    return this.http.get<LojaDTO[]>(`${environment.baseUrl}/lojas/?status=I`);
  }

  findById(id_loja : string) {
    return this.http.get<LojaDTO>(`${environment.baseUrl}/lojas/${id_loja}`);
  }

  findLojaByProduto(id_produto : string) {
    return this.http.get<LojaDTO>(`${environment.baseUrl}/lojas/produto/${id_produto}`);
  }

  aprovarLoja(selected: LojaDTO) {
    return this.http.put(
        `${environment.baseUrl}/lojas/aprovar/${selected.id}`,
        selected,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

  reprovarLoja(selected: LojaDTO) {
    return this.http.put(
        `${environment.baseUrl}/lojas/reprovar/${selected.id}`,
        selected,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 
}
