import { Injectable } from '@angular/core';
import { STORAGE_KEYS } from 'app/config/storage.config';
import { Carrinho } from 'models/carrinho';
import { StoredUser } from 'models/stored_user';

@Injectable({
  providedIn: 'root'
})
export class WebstorageService {

  constructor() { }

  getLocalUser() : StoredUser {
    let usr = localStorage.getItem(STORAGE_KEYS.localUser);

    if (usr == null) {
        return null;
    } else {
       return JSON.parse(usr); 
    }
  }

  setLocalUser(obj : StoredUser) {
    if (obj == null) {
        localStorage.removeItem(STORAGE_KEYS.localUser);
    } else {
        localStorage.setItem(STORAGE_KEYS.localUser, JSON.stringify(obj));
    }
  }

  getCarrinho() : Carrinho {
    let str = localStorage.getItem(STORAGE_KEYS.userCart);

    if (str == null) {
        return null;
    } else {
       return JSON.parse(str); 
    }
  }

  setCarrinho(obj : Carrinho) {
      if (obj == null) {
          localStorage.removeItem(STORAGE_KEYS.userCart);
      } else {
          localStorage.setItem(STORAGE_KEYS.userCart, JSON.stringify(obj));
      }
  }
}
