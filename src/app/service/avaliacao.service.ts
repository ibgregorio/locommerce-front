import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { AvaliacaoDTO } from 'models/avaliacao.dto';

@Injectable({
  providedIn: 'root'
})
export class AvaliacaoService {

  constructor(public http: HttpClient) { }

  insert(obj : AvaliacaoDTO) {
    return this.http.post(
        `${environment.baseUrl}/avaliacoes`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  }

}
