import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProdutoDTO } from 'models/produto.dto';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  constructor(public http: HttpClient) { }

  findProdutos(id_categoria: string, nome_produto: string) : Observable<ProdutoDTO[]> {
    if (id_categoria && nome_produto) {
      return this.http.get<ProdutoDTO[]>(`${environment.baseUrl}/produtos/?categoria=${id_categoria}&nome=${nome_produto}`);
    } else if (id_categoria) {
      return this.http.get<ProdutoDTO[]>(`${environment.baseUrl}/produtos/?categoria=${id_categoria}`);
    } else if (nome_produto) {
      return this.http.get<ProdutoDTO[]>(`${environment.baseUrl}/produtos/?nome=${nome_produto}`);
    } else {
      return this.http.get<ProdutoDTO[]>(`${environment.baseUrl}/produtos`);
    }
  }

  findById(id_produto : string) {
    return this.http.get<ProdutoDTO>(`${environment.baseUrl}/produtos/${id_produto}`);
  }

  loadProdutosFromLoja(id_loja : string) : Observable<ProdutoDTO[]> {
    return this.http.get<ProdutoDTO[]>(`${environment.baseUrl}/produtos/loja/${id_loja}`);
  }

  insert(obj : ProdutoDTO) {
    return this.http.post(
        `${environment.baseUrl}/produtos`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  }

  update(obj : ProdutoDTO, selected: ProdutoDTO) {
    return this.http.put(
        `${environment.baseUrl}/produtos/${selected.id}`,
        obj,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  } 

  delete(id: string) {
    return this.http.delete(
        `${environment.baseUrl}/produtos/${id}`,
        {
            observe: 'response',
            responseType: 'text'
        }
    );
  }
}
