import { Injectable } from '@angular/core';
import { Carrinho } from 'models/carrinho';
import { ProdutoDTO } from 'models/produto.dto';
import { WebstorageService } from './webstorage.service';

@Injectable({
  providedIn: 'root'
})
export class CarrinhoService {

  constructor(public storage: WebstorageService) { }

  createOrClearCarrinho() : Carrinho {
    let carrinho: Carrinho = {itens: []};
    this.storage.setCarrinho(carrinho);

    return carrinho;
  }

  getCarrinho() : Carrinho {
      let carrinho: Carrinho = this.storage.getCarrinho();
      if (carrinho == null) {
        carrinho = this.createOrClearCarrinho();
      }

      return carrinho;
  }

  adicionarProduto(produto: ProdutoDTO, qtd: number) : Carrinho {
      let carrinho = this.getCarrinho();
      let position = carrinho.itens.findIndex(x => x.produto.id == produto.id);

      if (position == -1) {
        if (qtd) {
          carrinho.itens.push({quantidade: qtd, produto: produto});
        } else {
          carrinho.itens.push({quantidade: 1, produto: produto});
        }
      }
      this.storage.setCarrinho(carrinho);

      return carrinho;
  }

  removerProduto(produto: ProdutoDTO) : Carrinho {
      let carrinho = this.getCarrinho();
      let position = carrinho.itens.findIndex(x => x.produto.id == produto.id);

      if (position != -1) {
        carrinho.itens.splice(position, 1);
      }
      this.storage.setCarrinho(carrinho);

      return carrinho;
  }

  aumentarQtd(produto: ProdutoDTO) : Carrinho {
      let carrinho = this.getCarrinho();
      let position = carrinho.itens.findIndex(x => x.produto.id == produto.id);

      if (position != -1) {
        carrinho.itens[position].quantidade++;
      }
      this.storage.setCarrinho(carrinho);

      return carrinho;
  }

  diminuirQtd(produto: ProdutoDTO) : Carrinho {
      let carrinho = this.getCarrinho();
      let position = carrinho.itens.findIndex(x => x.produto.id == produto.id);

      if (position != -1) {
        carrinho.itens[position].quantidade--;

        if (carrinho.itens[position].quantidade < 1) {
          carrinho = this.removerProduto(produto);
        }
      }
      this.storage.setCarrinho(carrinho);

      return carrinho;
  }

  total() : number {
      let carrinho = this.getCarrinho();
      let sum = 0;

      for (let i = 0; i < carrinho.itens.length; i++) {
          sum += carrinho.itens[i].produto.preco * carrinho.itens[i].quantidade;
      }

      return sum;
  }
}
