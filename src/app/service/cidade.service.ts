import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { CidadeDTO } from 'models/cidade.dto';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CidadeService {

  constructor(public http: HttpClient) {
  }

  findAll(id_estado : number) : Observable<CidadeDTO[]> {
      return this.http.get<CidadeDTO[]>(`${environment.baseUrl}/estados/${id_estado}/cidades`);
  }
}
